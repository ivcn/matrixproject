#ifndef KADR6_H
#define KADR6_H

#define SMMIN 8 //��������� ����� ��
#define SMMAX 4 //������������ ���������� ������������
#define PLMAX 8 //���������� ���������� � �����������

// ���������� ������� � "X" � "Y" ����������
#define MAXXSTRIP 320
#define MAXYSTRIP 288

//��������� ������ ����� ����������
#pragma pack(push,1)
#ifndef dTDateTimeKadr
#define dTDateTimeKadr
//��������� ��� �������� ���� � ������� � ���� ������� ���
//��������� �� ��������� ������, �����
struct TDateTimeKadr
{
 union{
  struct{
  unsigned char hsecond; //����� �������
  unsigned char second;  //�������
  unsigned char minute;  //������
  unsigned char hour; //���
  }tm;
  unsigned long time;
 };
 union{
  struct{
  unsigned char day; //����
  unsigned char month; //�����
  unsigned short year; //���
  }dt;
  unsigned long date;
 };
};
#endif

struct TKadr
{
  char Signature[8];  //"URAGAN6" ������ �����
  unsigned long size; //������ ������ ��������� � ������
  //��������� �����
  unsigned long Nrun; // ����� ����
  unsigned long Nkadr; //����� �����
  unsigned long Ntrigger; //���������� ��������� �� ����� ������ �����
  unsigned long Nevents; //���������� �������, ������������������, � �����
  unsigned long Nevbound; //Bound - ���������� �������, ������������������, � �����
  unsigned long NevboundX; //Bound - ���������� �������, ������������������, � �����
  unsigned long NevboundY; //Bound - ���������� �������, ������������������, � �����
  unsigned long ExpozTime;//������ ����� ���������� � �������������
  unsigned long LiveTime;//����� ����� ���������� � �������������
  unsigned long DecodeErr; // ���������� ������ ��������������
  TDateTimeKadr start; //����� ������ ���������� �����
  TDateTimeKadr stop;  //����� ����� ���������� �����
  unsigned long Pbar; //����������� �������� � mbar*1000.
  long Themperature; //����������� � ����*1000.
  unsigned short Noise[8]; //���� ����������
  unsigned short Monit[4][8]; //���������� ������ ��� ���������������
  unsigned long TrackPlane[2][8]; //[x,y][plane]���������� ������������ ���������� �� �����
  unsigned long NLam[8]; //���������� LAM��
  unsigned short Hit[8][608]; //Hit of "Y"plane,"X"plane
	//������� ��� Bound
	short StripXmin,StripXmax,StripYmin,StripYmax,PLmin,PLmax;
	//In DECOR coordinate
	unsigned short LXfull,LYfull,LZfull; //full box size in mm
	unsigned short LXbound,LYbound,LZbound; //bound box size in mm
	//Coordinate cross strip (0,0) on plane 0 in NEVOD coordinate, in mm
	short X0, Y0, Z0;
	// for convert from local coordinate DECOR to coordinate NEVOD *10000
	short VdcrX[3], VdcrY[3], VdcrZ[3]; //direct of: step Y strip, step plane, step X strip
  unsigned short Sm;//����� �����������
  char iddata[2]; // ������������� ���� ������
					 // 0-[theta][phi/4]
					 // 1-[45*sin(phi)sin(theta)+45][45*cos(phi)sin(theta)+45]
					 // 2-[45*sin(phi)theta/90+45][45*cos(phi)theta/90+45]
					 // 3-[45*tgY/6.+45][45*tgX/6.+45]
					 // 4-[45*atan(tgY)+45][45*atan(tgX)+45]
					 //    for Data2
					 // NoBound: 4-[tgY/da+45.5][tgX/da+45.5], da=tg(80*M_PI/180)/45.5
					 // Bound: 4-[atan(tgY)/2+45.5][atan(tgX)/2+45.5]
  unsigned short lendata; //����� ������� ������ Data + Data2
  //����� ����������� � ������������ � iddata[0]
  unsigned char Data[2][91][91]; //[all,bound][][]
  //����� ����������� � ������������ � iddata[1]
  unsigned char Data2[2][91][91]; //[all,bound][][]
  //Centr matrix Data2[0] for iddata[1]=4, cells: 35-55
  //if( (iy >=35 && iy <=55) && (ix >=35 && ix <=55) )
  // low byte place into Data2[iy][ix]
  // high byte place into massiv Data, begin from Data[1][85][0]
  // Example: high byte cell Data2[0][iy][ix] placed into
  //              *(&Data[1][85][0]+(ix-35)+(iy-35)*21)
  // or: high byte cell Data2[0][iy][ix]
  //      y = 85 + ((ix-35)+(iy-35)*21)/91
  //      x = ((ix-35)+(iy-35)*21)%91
  //    placed into Data[1][y][x]
};
#pragma pack(pop)


#pragma pack(push,1)

//��������� ����� ������ (�������� ����������) � ����� ������ ��������� ������
struct TUrgData
{
//��������� ������
 char Signature[7]; //��������� "URAGAN"
 unsigned char MaskaSM; // ������� ����� �������������� � Kadrs ������
 short version; //������ ������
 short lenheader; //����� ��������� ������� � Signature �� ������ Kadrs
                  // ��� ���� �������� � ������ � ������
 unsigned long length;// ������ ������ ��������� � ������

 short HeightOnEarth; // ������ ��� ������� ���� 178 ������
 // ? ������ ���������� ?
//���� ����������� �� ������ � ������� ������
 float AngleEast;//=180.-55.274;

 TDateTimeKadr start;
 TDateTimeKadr stop;
 unsigned long Nrun; //����� ����
 unsigned long Nkadr; //����� �����
 //�� ������� ��������
 unsigned long Press; //�������� mbar*1000
 long Temperature; //����������� ������*1000
 unsigned long express[SMMAX];// ��������� �������-������� ������� ����� ��
 //����� ��������� ��
 TKadr Kadrs[SMMAX];
};
#pragma pack(pop)
#endif
