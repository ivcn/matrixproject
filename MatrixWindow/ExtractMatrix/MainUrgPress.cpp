//---------------------------------------------------------------------------

#pragma hdrstop
#include <io.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <dos.h>
#include <conio.h>
#include <math.h>
#include <iostream.h>
#include <fstream.h>
#include <sstream>
#include <dir.h>
#include <vector>

#include "kadr6.h"
#include "kadr6sum.h"
#include "kadr6norm.h"


//������ ������ line �� ������� columns
void ParseLineToCol(std::string line, std::vector<std::string> &columns);
//������ ������ ���� � ������� ������� U
int ParseDateTimeU(std::string dt,const char *dateformat,const char *timeformat,
                  TDateTimeKadr &datetime);

struct TDateTimeGran
{
  TDateTimeKadr dfrom; // � ����� �������
  TDateTimeKadr dto;   // �� ���� ������ �������
};
//��������� �� ����� ����� ���������������� ����������
std::vector <TDateTimeGran> MaskaInput[4];

//�������� ������ �����
void LoadMaska(char *filename, std::vector <TDateTimeGran> &Maska);

int idUseMask=0; //=1 - ���������� ����� ����� ��� ���������� ������

TDateTimeKadr LastObrabSum;


int idBound=1; //����, =1 - ��� ������������� Bound-������

//����� ��� ����������� ������������ �������� ������
class TSumData
{
  public:
   int Nsum[SMMAX]; //���������� ���������������� ������ ��
   int Nadd; //���������� ���������������� �������� �������
   //��� ���������� �������� �������� � �����������
   double Press;
   double Temperature;
   double Psm[SMMAX];
   double Tsm[SMMAX];
   unsigned long NrunStart[SMMAX];
   unsigned long NkadrStart[SMMAX];
   TDateTimeKadr LastStart; //����� ������ ���������� ����������������� �����

   TUrgDataSum DS; //������� ��������� ����
   TSumData()
   {
    Clear();
   }
   void Clear()
   {
    memset(&DS,0,sizeof(TUrgDataSum));
    memset(Nsum,0,sizeof(Nsum));
    memset(Psm,0,sizeof(Psm));
    memset(Tsm,0,sizeof(Tsm));
    memset(NrunStart,0,sizeof(NrunStart));
    memset(NkadrStart,0,sizeof(NkadrStart));
    Nadd=0;
    Press=0;
    Temperature=0;
   }
  //����������� � ����� ��������� ������
  // Nm - ����� � ������� ��� ��������� ������������
   TUrgDataSum *GetSum(TUrgData &D, int Nm)
   {
     //���� ������ �����������, �� �������� ��������� DS
     if(Nadd==0)
     {
       Clear();
       memcpy(DS.Signature,D.Signature,sizeof(D.Signature));
       DS.version=D.version;
       DS.lenheader=(int)DS.Kadrs-(int)&DS;
       DS.length=sizeof(TUrgDataSum);
       DS.HeightOnEarth=D.HeightOnEarth;
       DS.AngleEast=D.AngleEast;
       DS.start=D.start;
       DS.Nrun=D.Nrun;
       DS.Nkadr=D.Nkadr;
     }
     DS.MaskaSM |=D.MaskaSM; //��������������� �����
     LastStart=D.start;
     DS.stop=D.stop;
     Press +=D.Press;
     Temperature +=D.Temperature;
     Nadd++;
     //���� �� ��������� ��
     for(int Sm=0; Sm < SMMAX; Sm++)
     {
       if( (D.MaskaSM&(1<<Sm))==0)continue;
       //���������� ��������-�������
       if( (D.express[Sm]&0x080000000) !=0)continue;
       DS.express[Sm] |=D.express[Sm];

       TKadr &Q=D.Kadrs[Sm];
       TKadrSum &QS=DS.Kadrs[Sm];
       if(Q.LiveTime*1.e-6 < 50. || Q.LiveTime*1.e-6 > 60.)continue;
       if(Q.Themperature<=0)continue;
       if(Q.Nevbound < 50000)continue;
       int idpl=0;
       for(int ipl=0; ipl < 8; ipl++)
       {
         if(double(Q.TrackPlane[0][ipl])/Q.Nevbound < 0.7)
         {
          idpl=1;
          break;
         }
         if(double(Q.TrackPlane[1][ipl])/Q.Nevbound < 0.7)
         {
          idpl=1;
          break;
         }
       }
       if(idpl)continue;
       QS.Ntrigger +=Q.Ntrigger;
       QS.Nevents +=Q.Nevents;
       QS.Nevbound +=Q.Nevbound;
       QS.NevboundX +=Q.NevboundX;
       QS.NevboundY +=Q.NevboundY;
       QS.ExpozTime +=Q.ExpozTime;
       QS.LiveTime +=Q.LiveTime;
       QS.DecodeErr +=Q.DecodeErr;
       if(Nsum[Sm]==0)
       {
        QS.start=Q.start;
        NrunStart[Sm]=Q.Nrun;
        NkadrStart[Sm]=Q.Nkadr;
       }
       QS.stop=Q.stop;
       Psm[Sm] +=Q.Pbar;
       Tsm[Sm] +=Q.Themperature;
       unsigned long *sumdata=QS.Noise;
       unsigned long *enddata=sumdata+sizeof(QS.Noise)/sizeof(QS.Noise[0]);
       unsigned short *datas=Q.Noise;
       while(sumdata < enddata) *sumdata++ += *datas++ ;
       memcpy(QS.Monit,Q.Monit,sizeof(Q.Monit));
       sumdata=QS.TrackPlane[0];
       enddata=sumdata+sizeof(QS.TrackPlane)/sizeof(QS.TrackPlane[0][0]);
       unsigned long *datat=Q.TrackPlane[0];
       while(sumdata < enddata) *sumdata++ += *datat++ ;
       sumdata=QS.NLam;
       enddata=sumdata+sizeof(QS.NLam)/sizeof(QS.NLam[0]);
       datat=Q.NLam;
       while(sumdata < enddata) *sumdata++ += *datat++ ;
       sumdata=QS.Hit[0];
       enddata=sumdata+sizeof(QS.Hit)/sizeof(QS.Hit[0][0]);
       datas=Q.Hit[0];
       while(sumdata < enddata) *sumdata++ += *datas++ ;
	//������� ��� Bound
       QS.StripXmin=Q.StripXmin; QS.StripXmax=Q.StripXmax;
       QS.StripYmin=Q.StripYmin; QS.StripYmax=Q.StripYmax;
       QS.PLmin=Q.PLmin; QS.PLmax=Q.PLmax;
	//In DECOR coordinate
       QS.LXfull=Q.LXfull; QS.LYfull=Q.LYfull; QS.LZfull=Q.LZfull; //full box size in mm
       QS.LXbound=Q.LXbound; QS.LYbound=Q.LYbound; QS.LZbound=Q.LZbound; //bound box size in mm
	//Coordinate cross strip (0,0) on plane 0 in NEVOD coordinate, in mm
       QS.X0=Q.X0; QS.Y0=Q.Y0; QS.Z0=Q.Z0;
	// for convert from local coordinate DECOR to coordinate NEVOD *10000
        //direct of: step Y strip, step plane, step X strip
       memcpy(QS.VdcrX,Q.VdcrX,3*sizeof(Q.VdcrX));
       QS.Sm=Q.Sm;//����� ����������� 8..11
       QS.iddata[0]=Q.iddata[0]; // ������������� ���� ������
       QS.iddata[1]=Q.iddata[1]; // ������������� ���� ������
       QS.lendata=sizeof(QS.Data)+sizeof(QS.Data2);
       sumdata=QS.Data[0][0];
       enddata=sumdata+(sizeof(QS.Data)/sizeof(QS.Data[0][0][0])-91);
       unsigned char *data=Q.Data[0][0];
       while(sumdata < enddata) *sumdata++ += *data++ ;
       //������������ ������ �������
       sumdata=QS.Data[1][90];
       enddata=sumdata+8;
       unsigned long *dclass=(unsigned long *)&Q.Data[1][90][0];
       while(sumdata < enddata) *sumdata++ += *dclass++ ;

       sumdata=QS.Data2[0][0];
       enddata=sumdata+sizeof(QS.Data2)/sizeof(QS.Data2[0][0][0]);
       data=Q.Data2[0][0];
       while(sumdata < enddata) *sumdata++ += *data++ ;
      //�������� ����������� ������� ������� �� ������������ ���������
       if(Q.iddata[1]==4)
       {
         for(int iy=35; iy <= 55; iy++)
         {
          int iy1=iy-35;
          for(int ix=35; ix <=55; ix++)
          {
           int ix1=ix-35;
           int iy2 = 85 + (ix1+iy1*21)/91;
           int ix2 = (ix1+iy1*21)%91;
           QS.Data2[0][iy][ix] +=Q.Data[1][iy2][ix2]<<8;
           QS.Data[1][iy2][ix2]=0;
          }
         }
       }
       Nsum[Sm]++;
     }//Sm
     if(Nadd < Nm)return NULL;
     //�������� ������� ��������
     DS.Press=Press/Nadd+0.5;
     DS.Temperature=Temperature/Nadd+0.5;
     for(int Sm=0; Sm < SMMAX; Sm++)
     {
      if(Nsum[Sm]==0)
      {
       DS.MaskaSM =DS.MaskaSM&(~(1<<Sm));
       continue;
      }
      TKadrSum &QS=DS.Kadrs[Sm];
      QS.Pbar =Psm[Sm]/Nsum[Sm]+0.5;
      QS.Themperature =Tsm[Sm]/Nsum[Sm]+0.5;
      for(int i=0; i < 8; i++)
      {
       QS.Noise[i]=QS.Noise[i]/Nsum[Sm]+0.5;
      }
     }
     return &DS;
   }//GetSum
   TUrgDataSum *GetCurSum()
   {
     if(Nadd == 0)return NULL;
     //�������� ������� ��������
     DS.Press=Press/Nadd+0.5;
     DS.Temperature=Temperature/Nadd+0.5;
     for(int Sm=0; Sm < SMMAX; Sm++)
     {
      if(Nsum[Sm]==0)
      {
       DS.MaskaSM =DS.MaskaSM&(~(1<<Sm));
       continue;
      }
      TKadrSum &QS=DS.Kadrs[Sm];
      QS.Pbar =Psm[Sm]/Nsum[Sm]+0.5;
      QS.Themperature =Tsm[Sm]/Nsum[Sm]+0.5;
      for(int i=0; i < 8; i++)
      {
       QS.Noise[i]=QS.Noise[i]/Nsum[Sm]+0.5;
      }
     }
     return &DS;
   }
};
#define NINTERVAL 1  //���������� ���������� ��� ������������ ������
struct THWrite
{
   int h[SMMAX]; //��������� �� ����� ���������� �� ������������
};
THWrite hwrite[NINTERVAL]; //��������� ������ ��� �����������
int nSummator[NINTERVAL]={10}; //���������� �������� ������ � ������� ������������
int nSummMin[NINTERVAL]={8};//����������� ���������� ������ � ��������� ������������
TSumData SumData[NINTERVAL]; //��� ������������ �������� ������

//������� �������
//���������� ��� ��������� ���������� � ������
static std::string DIRALL="STATALL"; //��� ���������� ��� ������ � ������� �� ���� �����
static char NameMask[256]; //������ ��� ����� � ������
static char NameDirStart[256]; //������ ��� ���������� ������� ���������
static char NameDirWrite[256]; //������ ��� ���������� ���� ������������
static char NameDirSrc[256]; //������ ��� �������� ���������� c ������� �����
static char Obrabdir[256]; //�������������� ���������� ������ ����
static char NameRun[256]; //��� ���������� ����
static char NameYear[256]; //��� ���������� Year
static char NameMonth[256]; //��� ���������� Month
static char NameDay[256]; //��� ���������� Day
static short currentrun; //����� �������� ������

static char file_read[256];
static char buf[512];
// ��������� ������ �� ���������� DirRead -> ��������� � ���������� DirWrite
short Obrab_data(char *DirRead,char *DirWrite);
//TUrgDataSum KadrSum;
TUrgData Kadr;
TKadrNorm KSum;
unsigned long PrevPbar=0;
unsigned long PrevThemperature=0;
//�������� �������� ����� � ��� �� ����
int ThMin=0;
int ThMax=76;
int ThStep=1;
//���������� � ����� ������ �����������
char *Postfix[2]={"","B"};

//---------------------------------------------------------------------------
char LastObrabName[NINTERVAL][256]; // ��� ����� � �������� ����������
                                    // ������������� �����

//��������� ��� �������� ������� ���������� ������������� �����
struct TLastObrab
{
  TDateTimeKadr start;
  TLastObrab()
  {
   start.date=0; start.time=0;
  }
  int Load_Last(char *filename)
  {
    std::ifstream from(filename);
     if(!from)
       return 0;
    std::string s;
    std::getline(from,s,'\n');
    ParseDateTimeU(s,"DD-MM-YYYY","HH:MM:SS",start);
    from.close();
    return 1;
  }
  void Save_Last(char *filename)
  {
    int hand=open(filename,O_RDWR|O_CREAT|O_TEXT|O_TRUNC,S_IWRITE);
     if(hand!=-1)
     {
      sprintf(buf,"%02d-%02d-%04d %02d:%02d:%02d\n",start.dt.day,start.dt.month,start.dt.year,
              start.tm.hour,start.tm.minute,start.tm.second);
      write(hand,buf,strlen(buf));
      close(hand);
     }
    return;
  }
  int Compare_Last(TDateTimeKadr &curr)
  {
    if(curr.date < start.date)
      return 0;
    if(curr.date == start.date && curr.time < start.time)
      return 0;
    if(curr.date == start.date && curr.time == start.time)
      return 1;
    return 2;
  }
};
TLastObrab LastObrab[NINTERVAL];

//�������� ���������
std::string sDTBegin, sDTEnd;
TDateTimeKadr DTBegin,DTEnd,DTCurr;
int idInterval; //=1 - ������� ��������� ��������� �� �������
int UseInterval=0; //������������ �� ���� Interval?
unsigned char MaskaSm=15; // ������� ����� �� ��� �����������: 11-10-09-08
std::vector<std::vector<double> > sumx;
std::vector<std::vector<double> > sumy;
std::vector<std::vector<double> > sumxy;
std::vector<std::vector<double> > sumx2;
std::vector<std::vector<double> > sumy2;
std::vector<std::vector<int> > nsum;

//�������� ������������ ������� ��� ����� �� ���������
// yx[][1]=a*yx[][0]+b - ��� ������ ������������
static int lin_mnk(int nsm,int nteta, double *a,double *b)
{
static int m,n;
static double si,sj,wj,si2,sij,det;
static double a1,a2,sj2;
static double d,yt,cy;
static int nxy;

	si=sj=0.;
	wj=0.;
	si2=sj2=0.;
	sij=0.;

        nxy=nsum[nsm][nteta];
        wj=nxy;
        si=sumx[nsm][nteta];
        sj=sumy[nsm][nteta];
        sij=sumxy[nsm][nteta];
        si2=sumx2[nsm][nteta];
        sj2=sumy2[nsm][nteta];

	*a=0;
	*b=0;

	det=wj*si2-si*si;
	if(det == 0.)return 0;//�� ������� ���������������
        d=fabs(det);
   *a=(sij*wj-sj*si)/det; //������� �������� �����
   *b=(sj*si2-sij*si)/det;//������� �������� �����
   //�� ����������� �������� ���������� �� ������
        det=si*sj-wj*sij;
        if(det==0.)return 1; //������ � ����� ���������� ���������� a � b
        det=(si*si-wj*si2-sj*sj+wj*sj2)/det;
        a1=(-det+sqrt(det*det+4.))/2.;
        a2=(-det-sqrt(det*det+4.))/2.;
		  if(fabs(a1-(*a))<= fabs(a2-(*a)))*a=a1;
		  else *a=a2;
		  *b=sj/wj-(*a)*si/wj;
   return 1;
}
//��������� ������������� ������
void StoreSumData(THWrite &hout,TSumData &SumD, int isum);

#pragma argsused
int main(int argc, char* argv[])
{
  int done,donedir,handle,Nrun;
  short i,j,n;
  int donedirY,donedirM,donedirD;
  int curryear,currmonth,currday;
  struct ffblk ffile; // ��� ������ ������
  struct ffblk fdir; // ��� ������ ��� ����������
  struct ffblk fdirY; // ��� ������ ��� ���������� Year
  struct ffblk fdirM; // ��� ������ ��� ���������� Month
  struct ffblk fdirD; // ��� ������ ��� ���������� Day

//������� ��� �������� ���������� � ������
   if(argc < 2)
   {
m0:  printf("Usage: UrgPressTheta1m.exe name_dir idBound idInterval maska\r\nExample:\r\n"
    "  UrgPressTheta1m.exe \\\\UrgServer\\Urgdat6\\Urgbasa\\ 1 1 mask");
     printf("\r\nPress SPACE for exit...");
     getch();
     exit(1);
   }
   strcpy(NameDirSrc,argv[1]);
   if(NameDirSrc[strlen(NameDirSrc)-1]!='\\')goto m0;

 //������� ��� ���������� ������ ��������� ���������
 strcpy(NameDirStart,"X:\\");
 NameDirStart[0]='A'+getdisk();
 i=getcurdir(0, NameDirStart+3);
 if(i){printf("Error getcurdir\n"); exit(1); }
 if(argc > 2)
   idBound=atoi(argv[2]);
 else
   idBound=0;
 if(argc > 3)UseInterval=atoi(argv[3]);
 if(argc > 4)
 {
  for(int i=0; i < 4; i++)
  {
    sprintf(NameMask,"%s_%01d.mask",argv[4],i);
    LoadMaska(NameMask,MaskaInput[i]);
  }
  idUseMask=1;
 }
 else
   idUseMask=0;

 {
  sumx.resize(4);
  sumy.resize(4);
  sumxy.resize(4);
  sumx2.resize(4);
  sumy2.resize(4);
  nsum.resize(4);
  int nteta=0;
  for(int i=ThMin; i < ThMax; i+=ThStep )
  {
     nteta++;
  }
  for(int i=0; i < 4; i++)
  {
   sumx[i].resize(nteta);
   sumy[i].resize(nteta);
   sumxy[i].resize(nteta);
   sumx2[i].resize(nteta);
   sumy2[i].resize(nteta);
   nsum[i].resize(nteta);
  }
  for(int i=0; i < 4; i++)
  {
    for(int j=0; j < nteta; j++)
    {
      sumx[i][j]=0;
      sumy[i][j]=0;
      sumxy[i][j]=0;
      sumx2[i][j]=0;
      sumy2[i][j]=0;
      nsum[i][j]=0;
    }
  }
  //��������, ���� ����, ������������� �����
  int h=open("SumXY.bin",O_RDONLY|O_BINARY,S_IREAD);
  if(h !=-1)
  {
    for(int i=0; i < 4; i++)
    {
      for(int j=0; j < nteta; j++)
      {
        read(h,&sumx[i][j],sizeof(sumx[i][j]));
        read(h,&sumy[i][j],sizeof(sumy[i][j]));
        read(h,&sumxy[i][j],sizeof(sumxy[i][j]));
        read(h,&sumx2[i][j],sizeof(sumx2[i][j]));
        read(h,&sumy2[i][j],sizeof(sumy2[i][j]));
        read(h,&nsum[i][j],sizeof(nsum[i][j]));
      }
    }
    close(h);
  }
 }

 for(int isum=0; isum < NINTERVAL; isum++)
 {
   for(int Sm=0; Sm < SMMAX; Sm++)
    hwrite[isum].h[Sm]=-1;
 }
 idInterval=0;
 if(UseInterval)
 {
 //����� ���� ��������� ���������
  sprintf(buf,"Interval.");
  std::ifstream from(buf);
  if(from)
  {
   std::getline(from,sDTBegin,'\n');
   if(sDTBegin[0]!=';')
   {
    ParseDateTimeU(sDTBegin,"DD-MM-YYYY","HH:MM:SS",DTBegin);
    LastObrabSum = DTBegin;
   }
   else
   {
    DTBegin.date=0;
    DTBegin.time=0;
   }
   std::getline(from,sDTEnd,'\n');
   if(sDTEnd[0]!=';')
     ParseDateTimeU(sDTEnd,"DD-MM-YYYY","HH:MM:SS",DTEnd);
   else
   {
    DTEnd.date=0xFFFFFFFF;
    DTEnd.time=0;
   }
   from.close();
   idInterval=1;
  }
 }
  //�������� ���������� ��� �����������
  if(idInterval)
  {
   buf[0]=0;
   if(DTBegin.date !=0)
    sprintf(buf,"%04d%02d%02d%02d%02d",
          DTBegin.dt.year,DTBegin.dt.month,DTBegin.dt.day,
          DTBegin.tm.hour,DTBegin.tm.minute);
   if(DTEnd.date !=0xFFFFFFFF)
   {
    sprintf(buf+strlen(buf),"_%04d%02d%02d%02d%02d",
          DTEnd.dt.year,DTEnd.dt.month,DTEnd.dt.day,
          DTEnd.tm.hour,DTEnd.tm.minute);
     DIRALL=buf;
   }
   if(buf[0]==0)
     idInterval=0;
  }
 if(idInterval==0 ||DTEnd.date==0xFFFFFFFF)
 {
  for(int isum=0; isum < NINTERVAL; isum++)
  {
   sprintf(LastObrabName[isum],"Last%d.dat",nSummator[isum]);
   if(LastObrab[isum].Load_Last(LastObrabName[isum]))
   {
    idInterval=0;
   }
  }
 }

  done=findfirst(DIRALL.c_str(),&fdir,FA_DIREC); //���� �� ����� ��� ?
  findclose(&fdir);
  if(done) //����� ���
  {
    mkdir(DIRALL.c_str()); //������
    done=findfirst(DIRALL.c_str(),&fdir,FA_DIREC); //���������
    findclose(&fdir);
    if(done) // ���������� �� �������
    {
      printf("No make subdir %s",DIRALL.c_str()); exit(1);
    }
  }
         //���������� ��� ���������� ��� ������ ���� ������
         strcpy(NameDirWrite,NameDirStart);
         strcat(NameDirWrite,"\\");
         strcat(NameDirWrite,DIRALL.c_str());
         chdir(NameDirWrite);
//����� ������������� ���������� NameDirSrc
 strcpy(file_read,NameDirSrc);
 strcat(file_read,"\\*.*");
// ������ ���� ����� ������������� Year
  donedirY=findfirst(file_read,&fdirY,FA_DIREC);
// ���� �� �������������� Year
  DTCurr.date=0; DTCurr.time=0;
  while (!donedirY)
  {
	 if( (fdirY.ff_attrib&FA_DIREC)==0)goto m1;
	 if(strcmp(fdirY.ff_name,".")==0 ||strcmp(fdirY.ff_name,"..")==0)
	 {
		goto m1;
	 }
	 //���������� ��� ���������� ������ ����� ������ ������
	 strcpy(file_read,NameDirSrc);
	 strcat(file_read,"\\");
	 strcat(file_read,fdirY.ff_name);
         strcat(file_read,"\\*.*");
         strcpy(NameYear,fdirY.ff_name);
         curryear=atol(NameYear);
         DTCurr.dt.year=curryear;
         if(idInterval==0)
         {
          int id=0;
          for(int isum=0; isum < NINTERVAL; isum++)
          {
           if(curryear < LastObrab[isum].start.dt.year)id++;
          }
          if(id==NINTERVAL)goto m1;
         }
         else
         {
          if(curryear < DTBegin.dt.year)goto m1;
          if(curryear > DTEnd.dt.year)break;
         }
         donedirM=findfirst(file_read,&fdirM,FA_DIREC);
         // ���� �� �������������� Month
         while (!donedirM)
         {
           if( (fdirM.ff_attrib&FA_DIREC)==0)goto m2;
	   if(strcmp(fdirM.ff_name,".")==0 ||strcmp(fdirM.ff_name,"..")==0)
	   {
		goto m2;
	   }
	   //���������� ��� ���������� ������ ����� ������ ������
	   strcpy(file_read,NameDirSrc);
	   strcat(file_read,"\\");
	   strcat(file_read,fdirY.ff_name);
	   strcat(file_read,"\\");
	   strcat(file_read,fdirM.ff_name);
           strcat(file_read,"\\*.*");
           strcpy(NameMonth,fdirM.ff_name);
         currmonth=atol(NameMonth);
         DTCurr.dt.month=currmonth;
         if(idInterval==0)
         {
          int id=0;
          for(int isum=0; isum < NINTERVAL; isum++)
          {
           if(curryear == LastObrab[isum].start.dt.year)
           {
            if(currmonth < LastObrab[isum].start.dt.month)id++;
           }
          }
          if(id==NINTERVAL)goto m2;
         }
         else
         {
          if(curryear == DTBegin.dt.year)
          {
           if(currmonth < DTBegin.dt.month)goto m2;
          }
          if(curryear == DTEnd.dt.year)
          {
           if(currmonth > DTEnd.dt.month)break;
          }
         }
         donedirD=findfirst(file_read,&fdirD,FA_DIREC);
         // ���� �� �������������� Day
         while (!donedirD)
         {
           if( (fdirD.ff_attrib&FA_DIREC)==0)goto m3;
	   if(strcmp(fdirD.ff_name,".")==0 ||strcmp(fdirD.ff_name,"..")==0)
	   {
		goto m3;
	   }
	   //���������� ��� ���������� ������ ����� ������ ������
	   strcpy(Obrabdir,NameDirSrc);
	   strcat(Obrabdir,"\\");
	   strcat(Obrabdir,fdirY.ff_name);
	   strcat(Obrabdir,"\\");
	   strcat(Obrabdir,fdirM.ff_name);
	   strcat(Obrabdir,"\\");
	   strcat(Obrabdir,fdirD.ff_name);
           strcpy(NameDay,fdirD.ff_name);
         currday=atol(NameDay);
         DTCurr.dt.day=currday;
         if(idInterval==0)
         {
          int id=0;
          for(int isum=0; isum < NINTERVAL; isum++)
          {
           if(curryear == LastObrab[isum].start.dt.year)
           {
            if(currmonth < LastObrab[isum].start.dt.month){id++; continue; }
            if(currmonth == LastObrab[isum].start.dt.month)
            {
             if(currday < LastObrab[isum].start.dt.day)id++;
            }
           }
          }
          if(id==NINTERVAL)goto m3;
         }
         else
         {
          if(DTCurr.date < DTBegin.date)goto m3;
          if(DTCurr.date > DTEnd.date)break;
         }
	  // ������� ����� ��������
	  strupr(NameDay);
	  strncpy(buf,NameDay,2);
          buf[3]=0;
	  currentrun=atol(buf);
          if(currentrun==0)goto m1; //����� �����
	  if(Obrab_data(Obrabdir,NameDirWrite))goto m1; //������
	  printf("\t%s-%s-%s\n",fdirD.ff_name,fdirM.ff_name,fdirY.ff_name);
          Nrun++;
m3:       donedirD = findnext(&fdirD); //���� ��������� �������������
         }
         findclose(&fdirD);
         if(idInterval)
          if(DTCurr.date > DTEnd.date)break;
m2:       donedirM = findnext(&fdirM); //���� ��������� �������������
         }
         findclose(&fdirM);
         if(idInterval)
           if(DTCurr.date > DTEnd.date)break;
m1: donedirY = findnext(&fdirY); //���� ��������� �������������
  }
  findclose(&fdirY);
  printf("End of data\n");

   sprintf(buf,"Dbeta%d%s.dat",nSummator[0],Postfix[idBound]);
   int hand_out=open(buf,O_RDWR|O_CREAT|O_TRUNC|O_TEXT,S_IWRITE);
   strcpy(buf,"Teta\tSinTeta\tfSm08\tbfSm08\tfSm09\tbfSm09\tfSm10\tbfSm10\tfSm11\tbfSm11");
   write(hand_out,buf,strlen(buf));
   double Psred=0.;
   if(nsum[0][0])
    Psred=sumx[0][0]/nsum[0][0];
   else
    if(nsum[1][0])
     Psred=sumx[1][0]/nsum[1][0];
    else
     if(nsum[2][0])
      Psred=sumx[2][0]/nsum[2][0];
     else
      if(nsum[3][0])
       Psred=sumx[3][0]/nsum[3][0];
   sprintf(buf,"\tPsred=%.3lf\n",Psred);
   write(hand_out,buf,strlen(buf));
   int iteta=0;
   for(int Th=ThMin; Th < ThMax; Th+=ThStep )
   {
     sprintf(buf,"%ld\t%.5lf",Th,sin((Th+ThStep/2.)*M_PI/180.));
     write(hand_out,buf,strlen(buf));
     double a,b;
     for(int Sm=0; Sm<4; Sm++)
     {
      if(lin_mnk(Sm,iteta,&a,&b))
      {
        double sredF=sumy[Sm][iteta]/nsum[Sm][iteta];
        sprintf(buf,"\t%lf\t%lf",sredF,a);
      }
      else
      {
        strcpy(buf,"\t-\t-");
      }
      write(hand_out,buf,strlen(buf));
     }
     write(hand_out,"\n",1);
     iteta++;
   }
   close(hand_out);
  chdir(NameDirStart);
 {
  //�������� ������������� �����
  int h=open("SumXY.bin",O_RDWR|O_CREAT|O_BINARY,S_IREAD|S_IWRITE);
  if(h !=-1)
  {
    int nteta=0;
    for(int i=ThMin; i < ThMax; i+=ThStep )
    {
     nteta++;
    }
    for(int i=0; i < 4; i++)
    {
      for(int j=0; j < nteta; j++)
      {
        write(h,&sumx[i][j],sizeof(sumx[i][j]));
        write(h,&sumy[i][j],sizeof(sumy[i][j]));
        write(h,&sumxy[i][j],sizeof(sumxy[i][j]));
        write(h,&sumx2[i][j],sizeof(sumx2[i][j]));
        write(h,&sumy2[i][j],sizeof(sumy2[i][j]));
        write(h,&nsum[i][j],sizeof(nsum[i][j]));
      }
    }
    close(h);
  }
 }
 if(idInterval==0 || DTEnd.date==0xFFFFFFFF)
 {
  for(int isum=0; isum < NINTERVAL; isum++)
   LastObrab[isum].Save_Last(LastObrabName[isum]);
 }
 getch();
  _sleep(1);
 return 0;
}
//---------------------------------------------------------------------------
// ��������� ������ �� ���������� DirRead -> ��������� � ���������� DirWrite
short Obrab_data(char *DirRead,char *DirWrite)
{
//��������� �� ���� ����� ����� �������� �
// �������� ������ �� ������������� DirRead, ������ � ��� ������
// � ������ � ��������� ���� ������������� �� Phi ���������� �������
// ��������������� �� ����� ����� �����, ��� ��������� ��������� Theta
 char NameFileSearch[256];
 char NameFileRead[256];
 char DateStart[24],DateStop[24];
 int handle, done;
 int lenfile,lenread,lenwrite;
  struct ffblk ffile; // ��� ������ ������
 struct ffblk fdir; // ��� ������ ��� ����������

 char buf[80];
 for(int i=0; i < NINTERVAL; i++)
  SumData[i].Nadd=0;

    done=findfirst(DirWrite,&fdir,FA_DIREC); //���������
    findclose(&fdir);
    if(done) // ���������� �� �������
    {
      printf("No make subdir %s",DirWrite); return 1;
    }
    done=findfirst(DirRead,&fdir,FA_DIREC); //���������
    findclose(&fdir);
    if(done) // ���������� �� �������
    {
      printf("No make subdir %s",DirRead); return 1;
    }

 strcpy(NameFileSearch,DirRead);
 strcat(NameFileSearch,"\\*.urg");
 done=findfirst(NameFileSearch,&ffile,FA_ARCH);
 if(done)return 1;

 do
 {
   strcpy(NameFileRead,DirRead);
   strcat(NameFileRead,"\\");
   strcat(NameFileRead,ffile.ff_name);
   handle=open(NameFileRead,O_RDONLY|O_BINARY);
   if(handle==-1)
   {
     printf("\r\n   No open %s",NameFileRead); continue;
   }
   lenfile=filelength(handle);
   if(lenfile%sizeof(TUrgData) ){close(handle); continue;}
   //������ �����
   while(read(handle,&Kadr,sizeof(TUrgData))==sizeof(TUrgData))
   {
    if(memcmp(Kadr.Signature,"URAGAN",7))continue;//break;
    if(Kadr.version!=1)continue;//break;
    TDateTimeKadr st=Kadr.start;
    st.tm.hsecond=0;
    if(idInterval)
    {
      DTCurr.time=st.time;
      DTCurr.date=st.date;
      if(DTCurr.date < DTBegin.date || DTCurr.date > DTEnd.date)break;
      if(DTCurr.date == DTBegin.date)
         if(DTCurr.time < DTBegin.time)continue;
      if(DTCurr.date == DTEnd.date)
         if(DTCurr.time > DTEnd.time)break;
    }

     if(Kadr.Press==0)Kadr.Press=PrevPbar;
     PrevPbar=Kadr.Press;
     if(Kadr.Temperature==0)Kadr.Temperature=PrevThemperature;
     PrevThemperature=Kadr.Temperature;
    //������� �����
   if(idUseMask)
    for(int Sm=0; Sm < 4; Sm++)
    {
     unsigned long express=Kadr.express[Sm];
     size_t n=MaskaInput[Sm].size(); //����� ������� �����
     if(n==0)continue;
     for(size_t i=0; i < n; i++)
     {
       TDateTimeGran &dg=MaskaInput[Sm][i];
      if(st.date < dg.dfrom.date || st.date > dg.dto.date)continue;

      if(st.date == dg.dfrom.date)
         if(st.time < dg.dfrom.time)continue;
      if(st.date == dg.dto.date)
         if(st.time > dg.dto.time)continue;
      express |=0x080000000;
      break;
     }
     Kadr.express[Sm]=express;
    }

  for(int isum=0; isum < NINTERVAL; isum++)//�� ���������� ������������
  {
    if(idInterval==0)
    {
     int ilast=LastObrab[isum].Compare_Last(st);
     if(ilast==0)continue; //����� ����� ������ ���������� �������������
     if(ilast==1)continue; //����� ����� ����� � ��������� ������������
    }
     //�������� ��������� �� ������� ���� � ��� �� ������ �������, ���
     // � ������ � �����
     TUrgDataSum *KS=NULL;
     if(SumData[isum].Nadd && SumData[isum].Nsum)
     {
      TDateTimeKadr dstart=SumData[isum].DS.start;
      TDateTimeKadr dstop=SumData[isum].DS.stop;
      dstart.tm.hsecond=0; dstart.tm.second=0;
      dstop.tm.hsecond=0; dstop.tm.second=0;
      bool krit;
        krit=dstart.date != dstop.date || dstart.tm.hour != dstop.tm.hour;
      if(nSummator[isum] < 60)
        krit=krit || (dstart.tm.minute/nSummator[isum]) !=(dstop.tm.minute/nSummator[isum]);
      if(krit)
      {
       KS=SumData[isum].GetCurSum();
        if(KS==NULL || SumData[isum].Nadd < nSummMin[isum])
        {
         if(KS && SumData[isum].Nadd)
         {
          LastObrab[isum].start=SumData[isum].LastStart;
         }
         SumData[isum].Clear();
         continue;
        }
      }
     }
     //���� ���� �������� ������ ����� ������, �� ��������� ���������� �����
     if(KS)
     {
      if(SumData[isum].Nadd >= nSummMin[isum])
        StoreSumData(hwrite[isum],SumData[isum],isum);
      LastObrab[isum].start=SumData[isum].LastStart;
      SumData[isum].Clear();
     }
     //���������� ��������� ����
     KS=SumData[isum].GetSum(Kadr,nSummator[isum]);
     //��������, �� ������� �� ����� ����� �� �������� �������
     if(KS==NULL && SumData[isum].Nadd)
     {
      TDateTimeKadr dstart=SumData[isum].DS.start;
      TDateTimeKadr dstop=SumData[isum].DS.stop;
      dstart.tm.hsecond=0; dstart.tm.second=0;
      dstop.tm.hsecond=0; dstop.tm.second=0;
      int minstart=dstart.tm.minute;
      int minstop=dstop.tm.minute;
      bool krit;
        krit=dstart.date != dstop.date || dstart.tm.hour != dstop.tm.hour;
      if(nSummator[isum] < 60)
        krit=krit || (minstart/nSummator[isum]) !=(minstop/nSummator[isum]);
      if(krit)
      {
       KS=SumData[isum].GetCurSum();
       if(KS==NULL || SumData[isum].Nadd < nSummMin[isum])
       {
         if(KS && SumData[isum].Nadd)
         {
          LastObrab[isum].start=SumData[isum].LastStart;
         }
         SumData[isum].Clear();
         continue;
       }
      }
     }
     if(KS)
     {
      if(SumData[isum].Nadd >= nSummMin[isum])
       StoreSumData(hwrite[isum],SumData[isum],isum);
      LastObrab[isum].start=SumData[isum].LastStart;
      SumData[isum].Clear();
     }
    }//isum
   }//read
   close(handle);
 }while( (done=findnext(&ffile)) ==0 );
 for(int isum=0; isum < NINTERVAL; isum++)
 {
  for(int Sm=0; Sm < SMMAX; Sm++)
  {
   if(hwrite[isum].h[Sm]!=-1)
    close(hwrite[isum].h[Sm]);
   hwrite[isum].h[Sm]=-1;
  }
 }
 findclose(&ffile);
 return 0;
}
void StoreSumData(THWrite &hout, TSumData &SumData, int iSumm)
{

 char NameFileWrite[256];
 char TempFileWrite[256];
 TUrgDataSum &DS=SumData.DS; //������� ��������� ����
 int nSumm=nSummator[iSumm];
 int nSMin=nSummMin[iSumm];
  for(int Sm=0; Sm < 1; Sm++)
  {

     char Smnomnom[2];
     Smnomnom[0] = Sm+1+'0';
     Smnomnom[1] = 0;
     char* Smnom = Smnomnom;
 
     //cout<<Smnom;


   if((DS.MaskaSM&(1<<Sm))==0)continue;
   if(SumData.Nsum[Sm] < nSMin)continue;
   if(hout.h[Sm]==-1)
   {
    //��� �����������

   int minute=DS.start.tm.minute;
   if(nSumm==60)
       minute=0;
   else
       minute=minute-(minute%nSumm);


       strcpy(NameFileWrite,NameDirStart);
       strcat(NameFileWrite, "\\urgmatr\\sm");

       //char* Smnom;
       //Smnom = itoa(Sm+1, Smnom, 10);
       strcat(NameFileWrite, Smnom);
       sprintf(NameFileWrite+strlen(NameFileWrite),"\\%02d_%02d_%02d_%02d_%02d.bin",DS.start.dt.day,DS.start.dt.month,DS.start.dt.year%100,
       0,0);

       //cout<<NameFileWrite<<endl;


       //cout<<TempFileWrite;

    /*strcpy(NameFileWrite,NameDirWrite);
    sprintf(NameFileWrite+strlen(NameFileWrite),"\\%02d_%02d_%02d_%02d_%02d.bin",DS.start.dt.day,DS.start.dt.month,DS.start.dt.year%100,
          DS.start.tm.hour,minute);   */



     hout.h[Sm]=open(NameFileWrite,O_RDWR|O_CREAT|O_BINARY,S_IREAD|S_IWRITE);
     if(hout.h[Sm]==-1)continue;
   }



   int hand_out=hout.h[Sm];
   TKadrSum &QS=DS.Kadrs[Sm];

       //������� ����� ��������� ����� � ������ ������� �����������
    /* sprintf(buf,"%02d_%02d_%02d_%02d_%02d",
  	  DS.start.dt.day,DS.start.dt.month,DS.start.dt.year%100,
          DS.start.tm.hour,minute);    */
     /*write(hand_out,buf,strlen(buf));*/

     double LTime=QS.LiveTime*1.e-9;
       //������� ����������� ��������
      /* if(QS.Pbar)
        sprintf(buf,"\t%.3lf",QS.Pbar/1000.);
       else
        sprintf(buf,"\t0");
       write(hand_out,buf,strlen(buf));   */
    /* if(idBound)
      sprintf(buf,"\t%.2lf",QS.Nevbound/LTime);
     else
      sprintf(buf,"\t%.2lf",QS.Nevents/LTime);  */
   /*  write(hand_out,buf,strlen(buf));
     double Pbar=QS.Pbar/1000.;
        int iteta=0;
        for(int Th=ThMin; Th < ThMax; Th +=ThStep) //��������� �� ������
        {
         unsigned long NT=0;
         for(int j=0; j < 90; j++) //��������� �� �������
         {
          for(int i=Th; i < Th+ThStep; i++)
          {
            NT +=QS.Data[idBound][i][j];
          }
         }//�� �������
         double f=0;
         if(LTime)
         {
           f=NT/LTime;
           sumx[Sm][iteta] +=Pbar;
           sumy[Sm][iteta] +=f;
           sumxy[Sm][iteta] +=Pbar*f;
           sumx2[Sm][iteta] +=Pbar*Pbar;
           sumy2[Sm][iteta] +=f*f;
           nsum[Sm][iteta]++;
         }
         iteta++;

         sprintf(buf,"\t%.4lf",f);
         write(hand_out,buf,strlen(buf));

        }
       write(hand_out,"\n",1); */
     int raznost = 0;

     if (QS.start.date>LastObrabSum.date)     //Dobivaem nulyami posledniy obrabotannyy den
        {
            TDateTimeKadr EndDaySum;
            EndDaySum.date = LastObrabSum.date;
            EndDaySum.tm.hour = 23;
            EndDaySum.tm.minute = 50;

            int r1 = EndDaySum.date*24*60+EndDaySum.tm.hour*60+EndDaySum.tm.minute;
            int r2 = LastObrabSum.date*24*60+LastObrabSum.tm.hour*60+LastObrabSum.tm.minute;
            raznost = r1-r2;
            //cout<<"konec dnya raznost "<<raznost<<endl;


            if(raznost>10)
            {

                strcpy(TempFileWrite,NameDirStart);
                strcat(TempFileWrite, "\\urgmatr\\sm");
                //char* Smnomnom = "3";
                strcat(TempFileWrite, Smnom);
                sprintf(TempFileWrite+strlen(TempFileWrite),"\\%02d_%02d_%02d_%02d_%02d.bin",EndDaySum.dt.day,EndDaySum.dt.month,EndDaySum.dt.year%100,
                0,0);
                int tempfile = open(TempFileWrite,O_RDWR|O_APPEND|O_BINARY,S_IREAD|S_IWRITE);

                int kolnull = (raznost/10.0);
                cout<<"Dobavlyaem "<<kolnull<<" nuley v konec dnya' "<<(int)EndDaySum.dt.day<<"/"<<(int)EndDaySum.dt.month<<"/"<<EndDaySum.dt.year<<endl;
                for(int i=0; i<kolnull;i++)
                {
                    for(int j=0; j<(91*91);j++)
                    {
                        unsigned long tempdat1 = 0;
                        write(tempfile,&tempdat1,sizeof(unsigned long));
                    }
                }
                close(tempfile);
            }
            LastObrabSum = EndDaySum;

        }

     int r1 = QS.start.date*24*60+QS.start.tm.hour*60+QS.start.tm.minute;
     int r2 = LastObrabSum.date*24*60+LastObrabSum.tm.hour*60+LastObrabSum.tm.minute;
     raznost = r1-r2;
        //cout<<"raznost "<<raznost<<endl;

     while(raznost>=24*6*10)
     {

        TDateTimeKadr EndDaySum;
        EndDaySum.date = LastObrabSum.date+1;
        EndDaySum.tm.hour = 23;
        EndDaySum.tm.minute = 50;

        cout<<"Dobavlyaem den' "<<(int)EndDaySum.dt.day<<"/"<<(int)EndDaySum.dt.month<<"/"<<EndDaySum.dt.year<<endl;

        strcpy(TempFileWrite,NameDirStart);
        strcat(TempFileWrite, "\\urgmatr\\sm");
        //char* Smnomnom = "3";
        strcat(TempFileWrite, Smnom);
        sprintf(TempFileWrite+strlen(TempFileWrite),"\\%02d_%02d_%02d_%02d_%02d.bin",EndDaySum.dt.day,EndDaySum.dt.month,EndDaySum.dt.year%100,
        0,0);
        int tempfile = open(TempFileWrite,O_RDWR|O_CREAT|O_BINARY,S_IREAD|S_IWRITE);
        for(int i=0; i<24*6;i++)
        {
            for(int j=0; j<(91*91);j++)
            {
                unsigned long tempdat1 = 0;
                write(tempfile,&tempdat1,sizeof(unsigned long));
            }
        }
        close(tempfile);
        raznost=raznost-(24*6*10);
        //cout<<"raznost "<<raznost<<endl;
        LastObrabSum = EndDaySum;
     }

        // cout<<DTBegin.tm.
        /*cout<<"date "<<(int)QS.start.dt.year<<" "<<(int)QS.start.dt.month<<" "<<(int)QS.start.dt.day<<" "<<endl;
        cout<<"time "<<(int)QS.start.tm.hour<<" "<<(int)QS.start.tm.minute<<" "<<(int)QS.start.tm.second<<endl;
        cout<<endl; */

     if(raznost>10)
     {
        cout<<"raznost "<<raznost<<endl;
        cout<<"date "<<(int)QS.start.dt.year<<" "<<(int)QS.start.dt.month<<" "<<(int)QS.start.dt.day<<" "<<endl;
        cout<<"time "<<(int)QS.start.tm.hour<<" "<<(int)QS.start.tm.minute<<" "<<(int)QS.start.tm.second<<endl;
        cout<<endl;

        int kolnull = (raznost/10.0)-1;
        if ((LastObrabSum.time==DTBegin.time)&&(LastObrabSum.date==DTBegin.date))
           kolnull++;
        for(int i=0; i<kolnull;i++)
        {
            for(int j=0; j<(91*91);j++)
            {
                unsigned long tempdat1 = 0;
                write(hand_out,&tempdat1,sizeof(unsigned long));
            }
        }
     }

     LastObrabSum=QS.start;


     for(int i=0; i <= 90; i++) //��������� �� ������
         for(int j=0; j <= 90; j++) //��������� �� �������
            {
                unsigned long tempdat1 = QS.Data[1][i][j]/LTime;
                write(hand_out,&tempdat1,sizeof(unsigned long));
                /*if((j==45)&&(i==45))
                    cout<<QS.Data[1][i][j]/LTime<<' ';  */

            }

  }//Sm
}
void LoadMaska(char *filename, std::vector <TDateTimeGran> &Maska)
{
  char tformat[20]="HH:MM:SS.DDDD";
  Maska.clear();

  std::ifstream src(filename);
  if(!src)return ;

  std::vector<std::string> columns; //������� ������� ������ ��������� �����
  columns.clear();

  std::string srcline;
  //�������� �������
  getline(src,srcline,'\n');
  TDateTimeKadr dt;
  int n=0;
  //��������� ���������� �����
  while(!src.eof()) //������ ������ ���� �� ����� ����� �����
  {
    getline(src,srcline,'\n');
    if(src.bad())break;
    if(srcline.length() > 2)
    {
     //��������� �� �������
     ParseLineToCol(srcline,columns);
     if(columns.size()==2)
        n++;
    }
  }
  if(n==0)
  {
   src.close();
   return;
  }
  Maska.resize(n);
 //������������ � ������ �����
   src.clear();
   src.seekg(0,std::ios::beg);
  //���������� �������� �������
  getline(src,srcline,'\n');
  n=0;
  //�������� ������ Maska
  while(!src.eof()) //������ ������ ���� �� ����� ����� �����
  {
    getline(src,srcline,'\n');
    if(src.bad())break;
    if(srcline.length() > 2)
    {
     //��������� �� �������
     ParseLineToCol(srcline,columns);
     if(columns.size()!=2)continue;
     //�������� ����� ���������� ��������� � ������� �����
     ParseDateTimeU(columns[0],"DD-MM-YYYY",tformat,Maska[n].dfrom);
     ParseDateTimeU(columns[1],"DD-MM-YYYY",tformat,Maska[n].dto);
     n++;
    }
  }
  src.close();

}
//������ ������ line �� ������� columns
void ParseLineToCol(std::string line, std::vector<std::string> &columns)
{
   int len=line.length();
   columns.clear();
   if(len==0)return;
   columns.push_back(line);
   //������� ��������������� ������ � ������
   // � �������� � ����������� ����� �� �����������
   for(size_t i=0; i < columns.size(); i++)
   {
      //���� ����������� � ������ columns[i]
      len=columns[i].length();
      for(int j=0; j < len; j++)
      {
        if(columns[i][j]=='\t')
        {
          //������� ����� ������ � ������
          columns.push_back(&columns[i][j+1]);
          //������� ������ �������
          columns[i].resize(j);
          break;
        }
      }
   }
}
//������ ������ ���� � ������� ������� U
int ParseDateTimeU(std::string dt,const char *dateformat,const char *timeformat,
                  TDateTimeKadr &datetime)
{
  memset(&datetime,0,sizeof(datetime));
   if(dt.length()< 2)return 0;
   //��������� ������ �� �����
    std::istringstream iss(dt);
   std::string sdate, stime; //���� � �����
    iss>>sdate>>stime;
   if(sdate[0] > '9')return 0; //������ �� ������
   //����������� � �����
   int ilen=sdate.length();
   int iformat=strlen(dateformat);
   char syear[6]={0},smonth[4]={0},sday[4]={0};
   int iyear=0,imonth=0,iday=0;
   for(int j=0; j < ilen; j++)
   {
     if(j > iformat)break;
     switch(dateformat[j])
     {
       case 'y':
       case 'Y':
              if(iyear < 4)
              {
               syear[iyear]=sdate[j];
               iyear++;
              }
                break;
       case 'm':
       case 'M':
              if(imonth < 2)
              {
               smonth[imonth]=sdate[j];
               imonth++;
              }
                break;

       case 'd':
       case 'D':
              if(iday < 2)
              {
               sday[iday]=sdate[j];
               iday++;
              }
                break;
       default:
               break;
     }
   }
   long d;
   if(iyear)
   {
    sscanf(syear,"%ld",&d);
    datetime.dt.year=d;
   }
   if(imonth)
   {
    sscanf(smonth,"%ld",&d);
    datetime.dt.month=d;
   }
   if(iday)
   {
    sscanf(sday,"%ld",&d);
    datetime.dt.day=d;
   }
   if(datetime.dt.year<2000) datetime.dt.year+=2000;
   //����������� � ��������
   ilen=stime.length();
   iformat=strlen(timeformat);
   char shour[4]={0},smin[4]={0},ssec[4]={0},shund[6]={0};
   int ihour=0,imin=0,isec=0,ihund;
   for(int j=0; j < ilen; j++)
   {
     if(j > iformat)break;
     switch(timeformat[j])
     {
       case 'h':
       case 'H':
              if(ihour < 2)
              {
               shour[ihour]=stime[j];
               ihour++;
              }
                break;
       case 'm':
       case 'M':
              if(imin < 2)
              {
               smin[imin]=stime[j];
               imin++;
              }
                break;

       case 's':
       case 'S':
              if(isec < 2)
              {
               ssec[isec]=stime[j];
               isec++;
              }
                break;
       case 'd':
       case 'D':
              if(ihund < 4)
              {
               shund[ihund]=stime[j];
               ihund++;
              }
                break;
       default:
               break;
     }
   }
   if(ihour)
   {
    sscanf(shour,"%ld",&d);
    datetime.tm.hour=d;
   }
   if(imin)
   {
    sscanf(smin,"%ld",&d);
    datetime.tm.minute=d;
   }
   if(isec)
   {
    sscanf(ssec,"%ld",&d);
    datetime.tm.second=d;
   }
   return 1;
}

